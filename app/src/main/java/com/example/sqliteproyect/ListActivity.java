package com.example.sqliteproyect;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.sqliteproyect.database.AgendaContacto;
import com.example.sqliteproyect.database.Contacto;

import java.util.ArrayList;

public class ListActivity extends android.app.ListActivity {
    private AgendaContacto agendaContacto;
    private Button btnNuevo;
    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        btnNuevo = (Button)findViewById(R.id.btnNuevo);
        agendaContacto = new AgendaContacto(this);

        llenarLista();
        adapter = new MyArrayAdapter(this,R.layout.layout_contacto,listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void llenarLista(){
        agendaContacto.openDataBase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }
    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter (Context context, int resource, ArrayList<Contacto> contactos){
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }
        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre  = (TextView)view.findViewById(R.id.lblNombreContacto);
            TextView lblTefefono = (TextView)view.findViewById(R.id.lblTelefonoContacto);

            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);

            if (contactos.get(position).isFavorite()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTefefono.setTextColor(Color.BLUE);
            }
            else{
                lblNombre.setTextColor(Color.BLACK);
                lblTefefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTefefono.setText(contactos.get(position).getTelefono1());
            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    agendaContacto.openDataBase();
                    agendaContacto.deleteContacto(contactos.get(position).getID());
                    agendaContacto.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });
            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;
        }
    }
}
