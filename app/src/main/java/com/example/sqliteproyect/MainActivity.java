package com.example.sqliteproyect;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sqliteproyect.database.AgendaContacto;
import com.example.sqliteproyect.database.AgendaDbHelper;
import com.example.sqliteproyect.database.Contacto;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    private  int id;
    EditText edtNombre;
    EditText edtTelefono;
    EditText edtTelefono2;
    EditText edtDireccion;
    EditText edtNotas;
    CheckBox cbxFavorito;
    private Contacto saveContact;
    int savedIndex;
    private AgendaContacto db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTelefono1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTelefono2);
        edtDireccion = (EditText) findViewById(R.id.edtDireccion);
        edtNotas = (EditText) findViewById(R.id.edtNotas);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);

        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);

        db = new AgendaContacto(MainActivity.this);

        saveContact = null;
        savedIndex = 0;
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(edtNombre.getText().toString().equals("") || edtDireccion.getText().toString().equals("")|| edtTelefono.getText().toString().equals("")) {
                Toast.makeText(MainActivity.this, R.string.mensajeerror, Toast.LENGTH_SHORT).show();
            }else{
                Contacto nContacto = new Contacto();
                nContacto.setNombre(edtNombre.getText().toString());
                nContacto.setTelefono1(edtTelefono.getText().toString());
                nContacto.setTelefono2(edtTelefono2.getText().toString());
                nContacto.setDomicilio(edtDireccion.getText().toString());
                nContacto.setNotas(edtNotas.getText().toString());
                if (cbxFavorito.isChecked()){
                    nContacto.setFavorito(1);
                }
                else{
                    nContacto.setFavorito(0);
                }
                db.openDataBase();
                if (saveContact == null){
                    long idx = db.insertarContacto(nContacto);
                    Toast.makeText(MainActivity.this, "Se agregó el contacto con el ID: " + idx,Toast.LENGTH_SHORT).show();
                }
                else{
                    db.UpdateContacto(nContacto,id);
                    Toast.makeText(MainActivity.this, "Se actualizó el registro: " + id, Toast.LENGTH_SHORT).show();
                }
                limpiar();
            }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i,0);
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            saveContact = contacto;
            id = contacto.getID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            if (contacto.isFavorite() > 0) {
                cbxFavorito.setChecked(true);
            }
        } else {
            limpiar();
        }
    }
    public void limpiar(){
        saveContact=null;
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
    }
}