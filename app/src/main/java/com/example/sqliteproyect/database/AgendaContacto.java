package com.example.sqliteproyect.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContacto {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;
    private String [] columnToRead = new String[]{
            DefinirTable.Contacto._ID,
            DefinirTable.Contacto.NOMBRE,
            DefinirTable.Contacto.TELEFONO1,
            DefinirTable.Contacto.TELEFONO2,
            DefinirTable.Contacto.DOMICILIO,
            DefinirTable.Contacto.NOTAS,
            DefinirTable.Contacto.FAVORITO
    };
    public AgendaContacto(Context context){
        this.context = context;
        agendaDbHelper = new AgendaDbHelper(this.context);
    }
    public void openDataBase(){
        db = agendaDbHelper.getWritableDatabase();
    }
    public long insertarContacto (Contacto c){
        ContentValues values = new ContentValues();
        values.put(DefinirTable.Contacto.NOMBRE,c.getNombre());
        values.put(DefinirTable.Contacto.TELEFONO1,c.getTelefono1());
        values.put(DefinirTable.Contacto.TELEFONO2,c.getTelefono2());
        values.put(DefinirTable.Contacto.DOMICILIO,c.getDomicilio());
        values.put(DefinirTable.Contacto.NOTAS,c.getNotas());
        values.put(DefinirTable.Contacto.FAVORITO,c.isFavorite());

        return db.insert(DefinirTable.Contacto.TABLE_NAME, null, values);
    }
    public long UpdateContacto (Contacto c, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTable.Contacto.NOMBRE,c.getNombre());
        values.put(DefinirTable.Contacto.TELEFONO1,c.getTelefono1());
        values.put(DefinirTable.Contacto.TELEFONO2,c.getTelefono2());
        values.put(DefinirTable.Contacto.DOMICILIO,c.getDomicilio());
        values.put(DefinirTable.Contacto.NOTAS,c.getNotas());
        values.put(DefinirTable.Contacto.FAVORITO,c.isFavorite());

        String criterio = DefinirTable.Contacto._ID + " = " + id;

        return db.update(DefinirTable.Contacto.TABLE_NAME, values, criterio, null);
    }
    public int deleteContacto(long id){
        String criterio = DefinirTable.Contacto._ID + " = " + id;
        return db.delete(DefinirTable.Contacto.TABLE_NAME, criterio, null);
    }
    public Contacto readContacto(Cursor cursor){
        Contacto c = new Contacto();
        c.setID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorito(cursor.getInt(6));

        return c;
    }
    public Contacto getContacto(long id){
        Contacto contacto = null;
        SQLiteDatabase db = agendaDbHelper.getReadableDatabase();
        Cursor c= db.query(DefinirTable.Contacto.TABLE_NAME, columnToRead, DefinirTable.Contacto._ID + " = ? ", new String[] {String.valueOf(id)}, null, null, null);
        if (c.moveToFirst()){
            contacto = readContacto(c);
        }
        c.close();
        return contacto;
    }
    public ArrayList<Contacto> allContactos(){
        ArrayList<Contacto> contactos = new ArrayList<Contacto>();
        Cursor cursor = db.query(DefinirTable.Contacto.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Contacto c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
    public void cerrar(){
        agendaDbHelper.close();
    }
}
