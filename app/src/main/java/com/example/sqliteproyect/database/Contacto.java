package com.example.sqliteproyect.database;

import java.io.Serializable;

public class Contacto implements Serializable {
    private int ID;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String domicilio;
    private String notas;
    private int favorito;

    public Contacto(int ID, String nombre, String telefono1, String telefono2, String domicilio, String notas, int favorito) {
        this.ID = ID;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.domicilio = domicilio;
        this.notas = notas;
        this.favorito = favorito;
    }

    public Contacto() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int isFavorite() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }
}
