package com.example.sqliteproyect.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AgendaDbHelper extends SQLiteOpenHelper {
    public static final String TEXT_TYPE = " TEXT";
    public static final String INTEGER_TYPE = " INTEGER";
    public static final String COMMA = " ,";
    public static final String SQL_CREATE_CONTACTO = " CREATE TABLE " + DefinirTable.Contacto.TABLE_NAME  +
            " (" + DefinirTable.Contacto._ID + " INTEGER PRIMARY KEY, " +
            DefinirTable.Contacto.NOMBRE + TEXT_TYPE + COMMA + DefinirTable.Contacto.DOMICILIO + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.TELEFONO1 + TEXT_TYPE + COMMA + DefinirTable.Contacto.TELEFONO2 + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.NOTAS + TEXT_TYPE + COMMA + DefinirTable.Contacto.FAVORITO + INTEGER_TYPE + " )";
    public static final String SLQ_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + DefinirTable.Contacto.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "agenda.db";

    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_CONTACTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SLQ_DELETE_CONTACTO);
    }
}

